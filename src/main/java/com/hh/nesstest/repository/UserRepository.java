package com.hh.nesstest.repository;

import com.hh.nesstest.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(
            value = "SELECT * FROM user u WHERE u.customer_id = :customerId",
            nativeQuery = true
    )
    List<User> getUsersByCustomerId(Long customerId);

}
