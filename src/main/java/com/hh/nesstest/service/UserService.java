package com.hh.nesstest.service;

import com.hh.nesstest.model.Customer;
import com.hh.nesstest.model.User;
import com.hh.nesstest.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<User> getUsersByCustomerId(Long customerId) {
        return userRepository.getUsersByCustomerId(customerId);
    }
}
