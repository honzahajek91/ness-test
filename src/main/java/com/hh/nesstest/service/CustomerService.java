package com.hh.nesstest.service;

import com.hh.nesstest.dto.CustomerDto;
import com.hh.nesstest.dto.CustomerDtoMapper;
import com.hh.nesstest.model.Customer;
import com.hh.nesstest.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomerService {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private CustomerDtoMapper customerDtoMapper;

    public List<CustomerDto> getCustomers() {
        return customerRepository.findAll()
                .stream()
                .map(customerDtoMapper).collect(Collectors.toList());
    }

    public Customer getCustomer(Long id) {
        return customerRepository.getReferenceById(id);
    }
}
