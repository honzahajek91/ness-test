package com.hh.nesstest.controller;

import com.hh.nesstest.dto.CustomerDto;
import com.hh.nesstest.model.Customer;
import com.hh.nesstest.service.CustomerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.tags.Tags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name="Customer")
@RestController
@RequestMapping(path = "/api")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Operation(summary = "Get all customers")
    @GetMapping(path = "/customer")
    List<CustomerDto> getCustomers() {
        return customerService.getCustomers();
    }

    @Operation(summary = "Get customer by id")
    @GetMapping(path = "/customer/{id}")
    Customer getCustomer(@PathVariable("id") Long id) {
        return customerService.getCustomer(id);
    }
}
