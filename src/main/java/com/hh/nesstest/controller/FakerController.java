package com.hh.nesstest.controller;

import com.github.javafaker.Faker;
import com.hh.nesstest.model.*;
import com.hh.nesstest.repository.*;
import io.swagger.v3.oas.annotations.Hidden;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashSet;
import java.util.Set;

@Hidden
@RestController
@RequestMapping(path = "/faker")
public class FakerController {

    @Autowired
    private CustomerRepository customerRepository;
    @Autowired
    private ContractRepository contractRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private ContactRepository contactRepository;
    @Autowired
    private AddressRepository addressRepository;

    @GetMapping
    public void fillData() {
        Faker faker = new Faker();

        for (int i = 0; i < 5; i++) {
            Customer customer = new Customer();
            customer.setName(faker.company().name());
            customer.setSapId(faker.number().randomNumber());

            Contact customerContact = new Contact();
            customerContact.setName(faker.name().fullName());
            customerContact.setEmail(faker.internet().emailAddress());
            customerContact.setPhone(faker.phoneNumber().phoneNumber());

            Address address = new Address();
            address.setName(faker.address().cityName());
            address.setPostalCode(faker.address().zipCode());
            address.setStreet(faker.address().streetAddress());

            addressRepository.save(address);
            customerContact.setAddress(address);

            contactRepository.save(customerContact);
            customer.setContact(customerContact);

            customerRepository.save(customer);

            for (int ii = 0; ii < 2; ii++) {
                User user = new User();
                Contact contact = new Contact();
                contact.setName(faker.name().fullName());
                contact.setEmail(faker.internet().emailAddress());
                contact.setPhone(faker.phoneNumber().phoneNumber());

                contactRepository.save(contact);
                user.setContact(contact);

                user.setCustomer(customer);
                userRepository.save(user);
            }

            for (int ii = 0; ii < 2; ii++) {
                Contract contract = new Contract();
                contract.setName(faker.book().title());
                contract.setNumber(String.valueOf(faker.number().randomNumber()));
                contract.setDescription(faker.book().genre());

                contract.setCustomer(customer);
                contractRepository.save(contract);
            }
        }
    }
}
