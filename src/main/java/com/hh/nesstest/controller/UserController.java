package com.hh.nesstest.controller;

import com.hh.nesstest.model.Customer;
import com.hh.nesstest.model.User;
import com.hh.nesstest.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Tag(name="User")
@RestController
@RequestMapping(path = "/api")
public class UserController {

    @Autowired
    private UserService userService;

    @Operation(summary = "Get all users by customer id")
    @GetMapping(path = "/user/customer/{customerId}")
    List<User> getUsersByCustomerId(@PathVariable("customerId") Long customerId) {
        return userService.getUsersByCustomerId(customerId);
    }
}
