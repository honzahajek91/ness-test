package com.hh.nesstest;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@OpenAPIDefinition(info=@Info(title="Ness test - OpenAPI", version="1.0"))
@SpringBootApplication
public class NessTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(NessTestApplication.class, args);
	}

}
