package com.hh.nesstest.dto;

import com.hh.nesstest.model.Contact;

public record CustomerDto(
        Long id,
        Long sapId,
        String name,
        Contact contact
) {
}
