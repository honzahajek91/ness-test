package com.hh.nesstest.dto;

import com.hh.nesstest.model.Customer;
import org.springframework.stereotype.Service;

import java.util.function.Function;

@Service
public class CustomerDtoMapper implements Function<Customer, CustomerDto> {
    @Override
    public CustomerDto apply(Customer customer) {
        return new CustomerDto(
                customer.getId(),
                customer.getSapId(),
                customer.getName(),
                customer.getContact()
        );
    }
}
